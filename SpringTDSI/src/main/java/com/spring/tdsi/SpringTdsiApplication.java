package com.spring.tdsi;

import com.spring.tdsi.models.Student;
import com.spring.tdsi.repository.StudentRepository;
import com.spring.tdsi.services.impl.StudentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTdsiApplication{

    public static void main(String[] args) {
        SpringApplication.run(SpringTdsiApplication.class, args);
    }

//    @Autowired
//    private StudentServiceImpl studentServiceImpl;
//
//    @Override
//    public void run(String... args) throws Exception {
//        Student student = new Student("WONE", "Mamadou", "201808GHW", "mamadu.wone@gmail.com", "774724175", "FST");
//        studentServiceImpl.saveStudent(student);
//    }
}
