package com.spring.tdsi.services;

import com.spring.tdsi.models.Student;

import java.util.List;

public interface StudentService {

    List<Student> getAllStudents();

    Student saveStudent(Student student);

    Student updateStudent(Student student);

    Student getStudentById(Long Id);

    void deleteStudent(Long Id);

    Student findStudentByNCE(String nce);

}
