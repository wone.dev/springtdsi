package com.spring.tdsi.services.impl;

import com.spring.tdsi.models.Student;
import com.spring.tdsi.repository.StudentRepository;
import com.spring.tdsi.services.StudentService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository) {
        super();
        this.studentRepository = studentRepository;
    }

    @Override
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    @Override
    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student updateStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student getStudentById(Long Id) {
        return studentRepository.findById(Id).get();
    }

    @Override
    public void deleteStudent(Long Id) {
        studentRepository.deleteById(Id);
    }

    @Override
    public Student findStudentByNCE(String nce) {
        return null;
    }
}
