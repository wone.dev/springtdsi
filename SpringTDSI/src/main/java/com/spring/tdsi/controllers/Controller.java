package com.spring.tdsi.controllers;

import com.spring.tdsi.models.Student;
import com.spring.tdsi.services.impl.StudentServiceImpl;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@org.springframework.stereotype.Controller
public class Controller {

    private final StudentServiceImpl studentServiceImpl;

    public Controller(StudentServiceImpl studentServiceImpl) {
        this.studentServiceImpl = studentServiceImpl;
    }

    @GetMapping(path = "/api/v1/students")
    public String listStudent(Model model){
        model.addAttribute("students", studentServiceImpl.getAllStudents());
        return "students";
    }

    @GetMapping("/api/v1/student/new")
    public String createStudent(Model model){
        Student student = new Student();
        model.addAttribute("student", student);
        return "create_student";
    }

    @PostMapping("/api/v1/students")
    public String saveStudent(@ModelAttribute("student") Student student){
        studentServiceImpl.saveStudent(student);
        String url = "/api/v1/students";
        return "redirect:"+url;
    }

    @GetMapping("/api/v1/student/edit/{id}")
    public String editStudent(@PathVariable Long id, Model model){
        model.addAttribute("student", studentServiceImpl.getStudentById(id));
        return "edit_student";
    }

    @PostMapping("/api/v1/student{id}")
    public String UpdateStudent(@PathVariable Long id, @ModelAttribute("student") Student student,
                                Model model){
        Student existingStudent = studentServiceImpl.getStudentById(id);
        existingStudent.setId(id);
        existingStudent.setLastName(student.getLastName());
        existingStudent.setFirstName(student.getFirstName());
        existingStudent.setEmail(student.getEmail());
        existingStudent.setFaculty(student.getFaculty());
        existingStudent.setPhoneNumber(student.getPhoneNumber());
        existingStudent.setNce(student.getNce());
        studentServiceImpl.updateStudent(existingStudent);
        String url = "/api/v1/students";
        return "redirect:"+url;
    }

    @GetMapping("/api/v1/student/{id}")
    public String deleteElector(@PathVariable Long id){
        studentServiceImpl.deleteStudent(id);
        String url = "/api/v1/students";
        return "redirect:"+url;
    }

}
